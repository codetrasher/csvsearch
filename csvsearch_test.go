package csvsearch

import (
	"os"
	"strings"
	"testing"
)

const (
	testDataPath = "testdata"
)

var expectedFilenames = [...]string{"ADABTC.csv", "CMTBTC.csv", "BCNBTC.csv", "BNBBTC.csv"}

func TestBegin(t *testing.T) {

	// Test invalid path
	_, err := Begin("testata")
	if !os.IsNotExist(err) {
		t.Fatal("Test should've returned an error.")
	}

	// Test with valid path and file names.
	csvs, err := Begin(testDataPath)
	if err != nil {
		t.Fail()
	}

	for i, name := range csvs.files {
		if !strings.Contains(name, expectedFilenames[i]) {
			t.Fatal("Didn't find expected file name.")
		}
	}
}
