package csvsearch

import (
	"os"
	"strings"
)

type CSVCollection struct {
	files []string
}

// Begin searches the given path for CSV files and returns the file names within
// CSVCollection struct and a nil error.
func Begin(path string) (*CSVCollection, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	// Parse file names
	defer f.Close()

	collection := &CSVCollection{files: make([]string, 0)}
	list, _ := f.Readdirnames(0)
	for _, name := range list {
		if strings.HasSuffix(name, ".csv") {
			collection.files = append(collection.files, path+"/"+name)
		}
	}

	return collection, nil
}
